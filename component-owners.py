#!/usr/bin/env python3
#
# Simple script to check owners of multiple components using DDB
# Copyright (C) 2016  Tomas Hozza
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import logging

from argparse import ArgumentParser
from subprocess import check_output
from subprocess import CalledProcessError


LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.StreamHandler())
LOGGER.setLevel(logging.INFO)


def get_owner_of_component(component, distro):
    try:
        if distro == 'rhel':
            output = check_output(['ddb', 'whoowns', '--product', 'Red Hat Enterprise Linux 7', component]).decode()
        else:
            output = check_output(['ddb', 'whoowns', component]).decode()
    except CalledProcessError:
        return None

    if distro == 'rhel':
        owner_re = re.compile(r'^.+? in Red Hat Enterprise Linux \d+ is owned by (.+?)$', re.MULTILINE)
    elif distro == 'fedora':
        owner_re = re.compile(r'^.+? in Fedora is owned by (.+?)( and comaintained by .+)?$', re.MULTILINE)
    else:
        return None

    match = owner_re.search(output)

    if match is None:
        return None

    owner = match.group(1)

    if owner == 'nobody':
        return None
    else:
        return owner


def main(options):
    component_to_owner = dict()
    unique_owners = set()

    with open(options.components) as f:
        for component in f.read().splitlines():
            component_to_owner[component] = get_owner_of_component(component, options.distro)

    LOGGER.info("Checked owners of %d components in '%s'", len(component_to_owner), options.distro)
    for component, owner in sorted(component_to_owner.items()):
        if owner is None:
            LOGGER.info('%s - DOES NOT EXIST', component)
        else:
            LOGGER.info('%s - %s', component, owner)
            unique_owners.add(owner)

    LOGGER.info('\nThere are %d unique owners:', len(unique_owners))
    for owner in sorted(unique_owners):
        LOGGER.info(owner)


if __name__ == '__main__':
    parser = ArgumentParser(description='Simple script to check owners of multiple components using DDB')
    parser.add_argument('-d', '--distro',
                        choices=['rhel', 'fedora'],
                        default='rhel',
                        help='Distribution in which to check for owners')
    parser.add_argument('-c', '--components',
                        required=True,
                        metavar='FILE',
                        help='Path to file with list of components to check')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        default=False,
                        help='More verbose output')
    #parser.add_argument()

    options = parser.parse_args()
    if options.verbose:
        LOGGER.setLevel(logging.DEBUG)
    main(options)
