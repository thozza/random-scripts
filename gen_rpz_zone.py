#!/usr/bin/env python3
#
# simple script to generate random RPZ zone for bind of an arbitrary size
# Copyright (C) 2015  Tomas Hozza
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import random
import string
import sys
import contextlib
import ipaddress


LOGGER = logging.getLogger(__file__)

GENERATORS = list()

HEADER = """$TTL 1H
@\t\tSOA LOCALHOST. named-mgr.example.com (1 1h 15m 30d 2h)
\t\tNS  LOCALHOST.

"""


class MetaRpzRecordGenerator(type):
    """
    Meta class which will add each generator class to the GENERATORS list
    """
    
    def __new__(mcs, name, bases, namespace):
        cls = super(MetaRpzRecordGenerator, mcs).__new__(mcs, name, bases, namespace)

        if name != 'RpzRecordGeneratorBase':
            GENERATORS.append(cls)
        return cls


class RpzRecordGeneratorBase(metaclass=MetaRpzRecordGenerator):
    """
    Base class for all RPZ record generators
    """

    RECORD_TYPES = [
        ('CNAME', '.'),  # NXDOMAIN
        ('CNAME', '*.'),  # NODATA
        ('CNAME', '*.example.com'),  # LOCAL-DATA
        ('CNAME', 'rpz-passthru.'),  # PASSTHRU
    ]

    RECORD = '{dname}\t\t{rtype}\t{policy}'

    _generated_records = set()

    @staticmethod
    def gen_random_domain(max_labels=6, max_label_len=10):
        labels = random.randint(1, max_labels)
        domain = '.'.join(''.join(random.choice(string.ascii_lowercase)
                                  for c in range(random.randint(1, max_label_len)))
                          for l in range(labels))
        return domain

    @staticmethod
    def reverse_ip_address(address):
        return '.'.join(address.split('.')[::-1])

    @staticmethod
    def gen_random_ip4(prefix=None):
        address = '.'.join(str(random.randint(1, 255)) for b in range(4))

        if prefix is not None:
            net = ipaddress.IPv4Network('{addr}/{prefix}'.format(addr=address, prefix=prefix), strict=False)
            address = net.exploded.split('/')[0]

        return address

    @staticmethod
    def gen_random_ip4_prefix():
        return str(random.randint(1, 32))

    @staticmethod
    def gen_random_ip6(prefix=128):
        """
        Generate random IPv6 address, but in the form as used in zone files
        """
        address = '.'.join(''.join(random.choice('0123456789abcdef')
                                   for i in range(random.randint(1, 4)))
                           for b in range(8))

        if prefix is not None:
            net = ipaddress.IPv6Network('{addr}/{prefix}'.format(addr=address.replace('.', ':'),
                                                                 prefix=prefix),
                                        strict=False)
            address = str(net.network_address).replace('::', ':zz:').strip(':').replace(':', '.')

        return address

    @staticmethod
    def is_ip6_valid(address):
        if address.strip('.') == 'zz':
            return False
        else:
            return True

    @staticmethod
    def gen_random_ip6_prefix():
        return str(random.randint(1, 128))

    @classmethod
    def generate(cls):
        raise NotImplemented()

    @classmethod
    def is_record_unique(cls, record=None):
        if record is None:
            return False

        if record in cls._generated_records:
            return False
        else:
            cls._generated_records.add(record)
            return True


class RpzRecordGeneratorQname(RpzRecordGeneratorBase):
    RPZ_QNAME = '{domain}'

    @classmethod
    def generate(cls):
        policy = random.choice(cls.RECORD_TYPES)

        while True:
            dname = cls.RPZ_QNAME.format(domain=cls.gen_random_domain())
            # make sure record is unique
            if cls.is_record_unique(dname):
                break

        return cls.RECORD.format(
            dname=dname,
            rtype=policy[0],
            policy=policy[1]
        )


class RpzRecordGeneratorNsdname(RpzRecordGeneratorBase):
    RPZ_NSDNAME = '{domain}.rpz-nsdname'

    @classmethod
    def generate(cls):
        policy = random.choice(cls.RECORD_TYPES)

        while True:
            dname = cls.RPZ_NSDNAME.format(domain=cls.gen_random_domain())
            # make sure record is unique
            if cls.is_record_unique(dname):
                break

        return cls.RECORD.format(
            dname=dname,
            rtype=policy[0],
            policy=policy[1]
        )


class RpzRecordGeneratorIp4(RpzRecordGeneratorBase):
    RPZ_IP4 = '{prefixlen}.{address}.rpz-ip'

    @classmethod
    def generate(cls):
        policy = random.choice(cls.RECORD_TYPES)

        while True:
            prefix = cls.gen_random_ip4_prefix()
            dname = cls.RPZ_IP4.format(
                prefixlen=prefix,
                address=cls.reverse_ip_address(cls.gen_random_ip4(prefix)))
            # make sure record is unique
            if cls.is_record_unique(dname):
                break

        return cls.RECORD.format(
            dname=dname,
            rtype=policy[0],
            policy=policy[1]
        )


class RpzRecordGeneratorIp6(RpzRecordGeneratorBase):
    RPZ_IP6 = '{prefixlen}.{address}.rpz-ip'

    @classmethod
    def generate(cls):
        policy = random.choice(cls.RECORD_TYPES)

        while True:
            prefix = cls.gen_random_ip6_prefix()
            address = cls.reverse_ip_address(cls.gen_random_ip6(prefix))
            # TODO: Could be done in a more thoughtful way
            if not cls.is_ip6_valid(address):
                continue
            dname = cls.RPZ_IP6.format(
                prefixlen=prefix,
                address=address)
            # make sure record is unique
            if cls.is_record_unique(dname):
                break

        return cls.RECORD.format(
            dname=dname,
            rtype=policy[0],
            policy=policy[1]
        )


class RpzRecordGeneratorNsip4(RpzRecordGeneratorBase):
    RPZ_NSIP4 = '{prefixlen}.{address}.rpz-nsip'

    @classmethod
    def generate(cls):
        policy = random.choice(cls.RECORD_TYPES)

        while True:
            prefix = cls.gen_random_ip4_prefix()
            dname = cls.RPZ_NSIP4.format(
                prefixlen=prefix,
                address=cls.reverse_ip_address(cls.gen_random_ip4(prefix)))
            # make sure record is unique
            if cls.is_record_unique(dname):
                break

        return cls.RECORD.format(
            dname=dname,
            rtype=policy[0],
            policy=policy[1]
        )


class RpzRecordGeneratorNsip6(RpzRecordGeneratorBase):
    RPZ_NSIP6 = '{prefixlen}.{address}.rpz-nsip'

    @classmethod
    def generate(cls):
        policy = random.choice(cls.RECORD_TYPES)

        while True:
            prefix = cls.gen_random_ip6_prefix()
            address = cls.reverse_ip_address(cls.gen_random_ip6(prefix))
            # TODO: Could be done in a more thoughtful way
            if not cls.is_ip6_valid(address):
                continue
            dname = cls.RPZ_NSIP6.format(
                prefixlen=prefix,
                address=address)
            # make sure record is unique
            if cls.is_record_unique(dname):
                break

        return cls.RECORD.format(
            dname=dname,
            rtype=policy[0],
            policy=policy[1]
        )


# from http://stackoverflow.com/questions/17602878/how-to-handle-both-with-open-and-sys-stdout-nicely
@contextlib.contextmanager
def smart_open(filename=None):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = sys.stdout

    try:
        yield fh
    finally:
        if fh is not sys.stdout:
            fh.close()


def generate(outfile=None, records_num=20):
    """
    Generate given number of records and write the zone data to the given file

    :param outfile: path to a file. If not passes the zone will be printed to STDOUT
    :param records_num: number of records to generate
    :return: None
    """
    with smart_open(outfile) as zonefile:
        zonefile.writelines(HEADER)
        for r in range(records_num):
            zonefile.write('{}\n'.format(random.choice(GENERATORS).generate()))


if __name__ == '__main__':
    filename = None
    records = 20

    if len(sys.argv) > 3:
        print('Usage: {} [num of records] [filename]'.format(sys.argv[0]))
        sys.exit(1)

    try:
        records = int(sys.argv[1])
    except IndexError:
        pass

    try:
        filename = sys.argv[2]
    except IndexError:
        pass

    generate(filename, records)
