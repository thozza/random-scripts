#!/bin/bash

# Simple script for checking bugs that could be acked for specific version of RHEL
#
# Copyright (C) 2015 Tomas Hozza
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

usage()
{
    echo
    echo "USAGE:"
    echo "$0 <version> [--devel-ack] [--dry-run] [--user USER]"
    echo "The version is a version of RHEL to check,"
    echo "consisting of <major_version>.<minor_version> (e.g. 6.7)."
    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

DEV_ACK_BUGS=0
DRY_RUN=0
TEAM=

# check CLI options
VERSION="$1"
shift

if [[ ! $VERSION =~ ^[0-9]+\.[0-9]+$ ]]; then
   echo "ERROR version is not correct"
   usage
fi

while [ "$1" != "${1##--}" ]; do
    case "$1" in
        --devel-ack)
            DEV_ACK_BUGS=1
            shift
            ;;

        --dry-run)
            DRY_RUN=1
            shift
            ;;

        --user)
            TEAM="$2 $TEAM"
            shift 2
            ;;

        --user=?*)
            TEAM="$TEAM ${1#--branch=}"
            shift
            ;;

        *)
            echo "Unrecognized option $option"
            usage
            ;;
    esac
done

TEAM_FILE="team.txt"
# check that the file exists and is readable
if [ -r "$TEAM_FILE" ]; then
    TEAM="$TEAM $(tr "\n" " " < $TEAM_FILE)"
fi

if [ -z "$TEAM" ]; then
    echo "ERROR: you haven't specified any e-mail adresses"
    exit 1
fi

STATE="NEW,ASSIGNED"
RHEL_FLAG="rhel-$VERSION.0?"
DEV_ACK_FLAG="devel_ack+"
BUGZILLA_LINK="https://bugzilla.redhat.com/show_bug.cgi?id="

echo "Showing bugs proposed for RHEL $VERSION, which can be devel_acked"
echo "-----------------------------------------------------------------"

for user in $TEAM; do
    echo
    echo "USER: $user"
    echo "-----------"
    
    CMD="bugzilla query --boolean_query=\"flagtypes.name-notsubstring-devel_ack+\" --boolean_query=\"keywords-substring-Patch | keywords-substring-EasyFix\" --bug_status=\"$STATE\" --assigned_to=\"$user\" --flag \"$RHEL_FLAG\" --outputformat=\"$BUGZILLA_LINK%{id} %{status} - (%{keywords}) - %{summary}\""
    
    if [ "$DRY_RUN" -eq "1" ]; then
        echo "Would run:"
        echo "$CMD"
    else
        eval "$CMD"
    fi
done

if [ "$DEV_ACK_BUGS" -eq "1" ]; then
    echo
    echo
    echo "Granting devel_ack+ to bugs proposed for RHEL $VERSION"
    echo "------------------------------------------------------"

    COMMENT="Granting devel_ack+ for the proposed RHEL version, since this bug contains keywords 'EasyFix' or 'Patch'"

    for user in $TEAM; do
        echo
        echo "USER: $user"
        echo "-----------"
        
        BUG_IDS=$(bugzilla query --boolean_query="flagtypes.name-notsubstring-devel_ack+" --boolean_query="keywords-substring-Patch | keywords-substring-EasyFix" --bug_status="$STATE" --assigned_to="$user" --flag "$RHEL_FLAG" --outputformat="%{id}" | tr "\n" " ")
        echo "Bug IDs: $BUG_IDS"

        for id in $BUG_IDS; do
            CMD="bugzilla modify -f \"$DEV_ACK_FLAG\" --comment=\"$COMMENT\" -p \"$id\""

            if [ "$DRY_RUN" -eq "1" ]; then
                echo "Would run:"
                echo "$CMD"
            else
                eval "$CMD"
                echo "devel_ack+ granted to Bug #$id"
            fi
        done
    done
fi

