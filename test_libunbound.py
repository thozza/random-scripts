#!/usr/bin/python3

# Simple script for running libunbound python examples against py2 and py3 and comparing the output for testing purposes.
#
# Copyright (C) 2015 Tomas Hozza
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
import subprocess

example_scripts = [
    'async-lookup.py',
    'dns-lookup.py',
    'dnssec_test.py',
    'dnssec-valid.py',
    'example8-1.py',
    'idn-lookup.py',
    'mx-lookup.py',
    'ns-lookup.py',
    'reverse-lookup.py',
]

examples_location = '/usr/share/doc/python-unbound'

examples = [os.path.join(examples_location, x) for x in example_scripts]

# compare the examples output when running with Python2 and Python3

failed = []
for script in examples:
    fail = False
    print("Running... {}".format(script))

    try:
        py2_ret = subprocess.check_output(['/usr/bin/python2', script],
                                          stderr=subprocess.STDOUT,
                                          universal_newlines=True)
    except subprocess.CalledProcessError as e:
        fail = True
        py2_ret = e.output
        print("ERROR: Python 2 run of '{0}' failed with exit status {1}".format(os.path.basename(script),
                                                                                  e.returncode))

    try:
        py3_ret = subprocess.check_output(['/usr/bin/python3', script],
                                          stderr=subprocess.STDOUT,
                                          universal_newlines=True)
    except subprocess.CalledProcessError as e:
        py3_ret = e.output
        fail = True
        print("ERROR: Python 3 run of '{0}' failed with exit status {1}".format(os.path.basename(script),
                                                                                  e.returncode))

    different_output = False
    for line in zip(py2_ret.split('\n'), py3_ret.split('\n')):
        l2 = line[0].strip()
        l3 = line[1].strip()

        if l2.startswith('raw data'):
            l2_data = l2.lstrip('raw data:').strip().split(';')
            l3_data = l3.lstrip('raw data:').strip().split(';')
            if sorted(l2_data) != sorted(l3_data):
                fail = different_output = True
            continue

        if l2 != l3:
            fail = different_output = True

    if different_output:
        print("ERROR: different output of '{0}' for Py2 and Py3\n".format(os.path.basename(script)))
        print(py2_ret)
        print(py3_ret)

    print('{0}\t{1}'.format("FAIL" if fail else "OK", os.path.basename(script)))
