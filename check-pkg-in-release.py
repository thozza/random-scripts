#!/usr/bin/env python3

import sys
import argparse
import logging

from subprocess import check_output


LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.StreamHandler())
LOGGER.setLevel(logging.INFO)


def is_pkg_in_release(pkg_name, release='7'):
    CMD = ['brew', 'search', '-r', 'rpm']
    rpm_name = '^{pkg_name}\.el{release}\..*'
    CMD.append(rpm_name.format(
        pkg_name=pkg_name,
        release=release
    ))

    LOGGER.debug("Running '%s'", CMD)
    output = check_output(CMD)
    LOGGER.debug("Got output:\n%s", output)
    if output:
        return True
    else:
        return False


def get_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--pkg-list-file',
        required=True,
        help='Path to file which contains list of package names to check for existence in a release. One pkg a line'
    )
    parser.add_argument(
        '-r', '--rhel-version',
        choices=['6', '7'],
        default='7',
        help='RHEL major version for which to check the existence of a package'
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        default=False,
        help='More verbose output'
    )
    return parser


def main(args):
    arg_parser = get_arg_parser()
    options = arg_parser.parse_args(args)

    if options.verbose:
        LOGGER.setLevel(logging.DEBUG)

    # sort packages based on fact if they are in the release or not
    pkgs_in_release = list()
    pkgs_not_in_release = list()

    LOGGER.debug("Opening file '%s'", options.pkg_list_file)
    with open(options.pkg_list_file) as f:
        for pkg_raw_name in f:
            pkg_name = pkg_raw_name.strip().split()[0]
            LOGGER.debug("Checking package '%s' in RHEL '%s'", pkg_name, options.rhel_version)
            if is_pkg_in_release(pkg_name, options.rhel_version):
                pkgs_in_release.append(pkg_name)
            else:
                pkgs_not_in_release.append(pkg_name)

    LOGGER.info("Packages that exist in RHEL-%s:\n%s", options.rhel_version, '\n'.join(pkgs_in_release))
    LOGGER.info("\nPackages that DON'T exist in RHEL-%s:\n%s", options.rhel_version, '\n'.join(pkgs_not_in_release))


if __name__ == '__main__':
    main(sys.argv[1:])
