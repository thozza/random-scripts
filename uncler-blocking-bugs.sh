#!/bin/bash

# Simple script for generating list of bugs per assignee, which block passed Bug ID
# and don't have any Devel Cond NAKs set, nor the EasyFix or Patch.
#
# Copyright (C) 2015 Tomas Hozza
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

usage()
{
    echo
    echo "USAGE:"
    echo "$0 <blocked_bug_id>"
    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

# check CLI options
BLOCKED_BUG_ID="$1"
shift

if [[ ! $BLOCKED_BUG_ID =~ ^[0-9]+$ ]]; then
   echo "ERROR Bug ID is not valid"
   usage
fi

STATE="NEW,ASSIGNED,MODIFIED,POST"
BUGZILLA_LINK="https://bugzilla.redhat.com/show_bug.cgi?id="

NO_DEVEL_COND_NAK="cf_conditional_nak-notsubstring-Hardware & cf_conditional_nak-notsubstring-Upstream & cf_conditional_nak-notsubstring-Capacity & cf_conditional_nak-notsubstring-Patch & cf_conditional_nak-notsubstring-Design & cf_conditional_nak-notsubstring-Reproducer"
NO_DEVEL_ACK="flagtypes.name-notsubstring-devel_ack+"
NO_APPLICABLE_KW="keywords-notsubstring-Patch & keywords-notsubstring-EasyFix"

ASSIGNEES=$(bugzilla query --bug_status="$STATE" --blocked="$BLOCKED_BUG_ID" --boolean_query="$NO_DEVEL_COND_NAK" --boolean_query="$NO_DEVEL_ACK" --boolean_query="$NO_APPLICABLE_KW" --outputformat="%{assigned_to}" | sort | uniq)

echo "Assignees of bugs in unclear state:"
echo "-----------------------------------"
echo "$ASSIGNEES"
echo
echo


echo "Bugs per individual assignee:"
echo "-----------------------------"
echo
for assignee in $ASSIGNEES; do
    echo "${assignee}:"
    bugzilla query --bug_status="$STATE" --blocked="$BLOCKED_BUG_ID" --boolean_query="$NO_DEVEL_COND_NAK" --boolean_query="$NO_DEVEL_ACK" --boolean_query="$NO_APPLICABLE_KW" --assigned_to="$assignee" --outputformat="$BUGZILLA_LINK%{id} - %{summary}"
    echo
done

