#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the project nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import re
import logging

from argparse import ArgumentParser

import bugzilla


LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.StreamHandler())
LOGGER.setLevel(logging.INFO)


class BZilla(object):

    FLAG_REL_REQ = 'rhel-{major}.{minor}.0?'
    FLAG_REL_SET = 'rhel-{major}.{minor}.0+'
    FLAG_REL_NACK = 'rhel-{major}.{minor}.0-'
    FLAG_PM_RESET = 'pm_ack?'

    PRODUCT_RHEL = 'Red Hat Enterprise Linux {major}'

    BUG_STATES = ['NEW', 'ASSIGNED']

    BUG_LINK = 'https://bugzilla.redhat.com/{id}'

    def __init__(self, bzurl, dry_run=False):
        self.bzapi = bugzilla.Bugzilla(bzurl)
        self.dry_run = dry_run

    @staticmethod
    def split_release_version_str(version):
        """
        Splits the release version string to major and minor versions.

        :param version: String with the release version
        :return: Tuple of two numbers or tuple of None in case the version string is not valid.
        """
        version_re = re.compile(r'^\s*?(\d+?).(\d+?)\s*?$')
        match = version_re.search(version)

        if match is None:
            return None, None
        else:
            return int(match.group(1)), int(match.group(2))

    @staticmethod
    def check_release_version_str(version):
        """
        Check if the string is a valid version string.

        :param version: String with the release version
        :return: True if it is valid, False otherwise
        """
        major, minor = BZilla.split_release_version_str(version)
        if major is None or minor is None:
            return False
        else:
            return True

    def get_old_bugs_for_user(self, user, current_release):
        """
        Get list of bugs for a user, which are proposed for the old releases, but not for the current one.

        :param user: valid bugzilla user
        :param current_release: Current release version for which the bugs should be proposed
        :return: List with bug IDs
        """
        major_version, minor_version = self.split_release_version_str(current_release)

        flags = []
        # The idea is to look for bugs NOT proposed for current or later RHEL releases
        for i in range(minor_version, minor_version+10):
            flags.append(self.FLAG_REL_SET.format(major=major_version, minor=i))
            flags.append(self.FLAG_REL_NACK.format(major=major_version, minor=i))
            flags.append(self.FLAG_REL_REQ.format(major=major_version, minor=i))

        query = self.bzapi.build_query(
            product=self.PRODUCT_RHEL.format(major=major_version),
            assigned_to=user,
            status=self.BUG_STATES,
            flag=flags,
            booleantype='notsubstring',
            keywords=['ZStream', 'Security', 'Tracking'],
            keywords_type='nowords',
            include_fields=['id', 'status', 'summary', 'flags'],
        )

        LOGGER.debug("Executing Bugzilla query: '%s'", query)

        return self.bzapi.query(query)

    @staticmethod
    def _if_force_reset_devel_ack(bug):
        """
        Inspects the given bug and assesses if to reset the devel_ack even though the reset may be explicitly turned
        off.

        The reason to reset the flag may be that it was NACKed (IOW set to '-').

        :param bug: Bug object
        :return: True if to force the reset of devel_ack for the bug. False otherwise
        """
        for flag in bug.flags:
            if flag['name'] == 'devel_ack' and bool(flag['is_active']):
                flag_status = flag['status']
                if flag_status == '?' or flag_status == '+':
                    return False
        return True

    def move_old_bugs(self, old_bugs, current_release, reset_pm_ack=True, reset_devel_ack=True, reset_qa_ack=True):
        """
        Move the bugs list to the new release.

        :param old_bugs: list of bugs to move to the next RHEL release
        :type old_bugs: list
        :param current_release: string with the RHEL release in development (in form 'X.Y')
        :type current_release: str
        :return:
        """
        major_version, minor_version = self.split_release_version_str(current_release)

        new_flags = {
            'rhel-{major}.{minor}.0'.format(major=major_version, minor=minor_version): '?',
            'rhel-{major}.{minor}.0'.format(major=major_version, minor=minor_version-1): 'X',
        }
        if reset_pm_ack:
            new_flags['pm_ack'] = '?'
        if reset_devel_ack:
            new_flags['devel_ack'] = '?'
        if reset_qa_ack:
            new_flags['qa_ack'] = '?'

        bug_comment = 'Proposing for {}.{} for review during Planning Phase.'.format(major_version, minor_version)

        for bug in old_bugs:
            flags_to_set = dict(new_flags)

            if not reset_devel_ack and self._if_force_reset_devel_ack(bug):
                LOGGER.info("Forcing reset of devel_ack for bug #%d", bug.id)
                flags_to_set['devel_ack'] = '?'

            if self.dry_run:
                LOGGER.info("Would set flags on bug #%d to %s", bug.id, str(flags_to_set))
                LOGGER.info("Would update bug #%d with private comment: '%s'", bug.id, bug_comment)
            else:
                LOGGER.info("Updating bug #%d", bug.id)
                bug.addcomment(bug_comment, private=True)
                bug.updateflags(flags_to_set)


def main(options):
    release_version = options.REL_VERSION
    users_list = set(options.user)
    bz = BZilla(options.bz_url, options.dry_run)

    # check the release version
    if not bz.check_release_version_str(release_version):
        raise RuntimeError("Release version '{}' is not valid!".format(release_version))

    # combine users from command line and from file
    if options.users_list is not None:
        try:
            with open(options.users_list) as f:
                for line in f:
                    user = line.strip()
                    if user:
                        users_list.add(user)
        except FileNotFoundError:
            LOGGER.warning("Specified users list file '{}' does not exist!".format(options.users_list))

    LOGGER.debug("Will check bugs for users: %s", str(users_list))

    old_bugs = dict()

    # get old bugs for all users
    for user in sorted(users_list):
        old_bugs[user] = bz.get_old_bugs_for_user(user, release_version)

        bugs_str_list = [BZilla.BUG_LINK.format(id=bug.id) + " {}".format(bug.status) for bug in old_bugs[user]]

        LOGGER.info("Old bugs for user '%s':", user)
        if bugs_str_list:
            LOGGER.info('\n'.join(bugs_str_list) + '\n')
        else:
            LOGGER.info('NO OLD BUGS\n')

    # propose all old bugs for the current release
    if options.move:
        LOGGER.info("Moving old bugs to the current release (%s)", release_version)
        for user in users_list:
            LOGGER.info("Processing bugs for user: %s", user)
            bz.move_old_bugs(
                old_bugs[user],
                release_version,
                not options.no_reset_pm_ack,
                not options.no_reset_devel_ack,
                not options.no_reset_qa_ack
            )


if __name__ == '__main__':
    parser = ArgumentParser(description='Simple script to move bugs proposed for old releases to the given one.')
    parser.add_argument('REL_VERSION',
                        help='The current release version to which the bugs should be moved (X.Y).')
    parser.add_argument('-u', '--user',
                        action='append',
                        metavar='EMAIL',
                        default=list(),
                        help='BZ user for which to check bugs. You can specify multiple users')
    parser.add_argument('-l', '--users-list',
                        required=False,
                        metavar='FILE',
                        default=None,
                        help='Path to file with list of Bugzilla users')
    parser.add_argument('-m', '--move',
                        action='store_true',
                        default=False,
                        help='Whether to move the bugs, which could be moved.')
    parser.add_argument('--no-reset-pm-ack',
                        action='store_true',
                        default=False,
                        help='Whether to reset the pm_ack to ? when moving the bugs')
    parser.add_argument('--no-reset-devel-ack',
                        action='store_true',
                        default=False,
                        help='Whether to reset the devel_ack to ? when moving the bugs')
    parser.add_argument('--no-reset-qa-ack',
                        action='store_true',
                        default=False,
                        help='Whether to reset the qa_ack to ? when moving the bugs')
    parser.add_argument('--bz-url',
                        default='bugzilla.redhat.com',
                        help='Bugzilla URL to be used for actions')
    parser.add_argument('--dry-run',
                        action='store_true',
                        default=False,
                        help='Do not do any changes, just print what would be done')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        default=False,
                        help='More verbose output')

    options = parser.parse_args()
    if options.verbose:
        LOGGER.setLevel(logging.DEBUG)

    LOGGER.debug('Arguments: %s', str(options))

    try:
        main(options)
    except KeyboardInterrupt:
        LOGGER.info('Interrupted by user')
        sys.exit(0)
    except RuntimeError as e:
        LOGGER.critical(str(e))
        sys.exit(1)
