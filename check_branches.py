#!/usr/bin/env python3
#
# simple script to check existence of given branch for given set of components
# Copyright (C) 2015  Tomas Hozza
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import logging

from argparse import ArgumentParser
from subprocess import check_call
from subprocess import CalledProcessError
from subprocess import DEVNULL
from tempfile import TemporaryDirectory


logging.basicConfig(
    level=logging.DEBUG,
    format='%(levelname)-8s %(message)s',
    handlers=(logging.StreamHandler(),)
)
LOGGER = logging.getLogger(__name__)


class WorkingSpace(object):
    """
    Class managing the temporary working space
    """

    def __init__(self, cleanup_on_exit=False):
        self.cleanup_on_exit = cleanup_on_exit
        self.dir = TemporaryDirectory()

    @property
    def path(self):
        return self.dir.name

    def __enter__(self):
        return self.path

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.cleanup_on_exit:
            self.dir.cleanup()


def main(config):
    """
    The main function

    :param config: configuration as parsed by ArgumentParser
    :return: None
    """
    branches = set(config.branch)
    have_branch = {b: list() for b in branches}

    LOGGER.info("Reading components list from '%s'", config.comps_file)
    with open(config.comps_file, 'r') as f:
        components = set(f.read().splitlines())
        LOGGER.info("Read %d unique components", len(components))

    with WorkingSpace(config.cleanup) as workspace:
        LOGGER.info("Working directory: '%s'", workspace)

        # check all components
        for component in components:
            LOGGER.info("Checking component '%s'", component)
            try:
                check_call(['rhpkg', 'clone', component], stdout=DEVNULL, stderr=DEVNULL, cwd=workspace)
            except CalledProcessError:
                LOGGER.error("Could not clone component '%s', skipping", component)
                continue

            component_dir = os.path.join(workspace, component)

            # check branches for the component
            for branch in branches:
                LOGGER.info("Checking branch '%s'", branch)
                try:
                    check_call(['rhpkg', 'switch-branch', branch], stdout=DEVNULL, stderr=DEVNULL, cwd=component_dir)
                except CalledProcessError:
                    LOGGER.info("Component '%s' does not have branch '%s'", component, branch)
                else:
                    have_branch[branch].append(component)

    print()
    for branch in branches:
        print("Components WITH branch '{0}' - {1}:".format(branch, str(len(have_branch[branch]))))
        print('\n'.join(have_branch[branch]))
        print()

        without = list(components - set(have_branch[branch]))
        print("Components WITHOUT branch '{0}' - {1}:".format(branch, str(len(without))))
        print('\n'.join(without))
        print()


if __name__ == '__main__':

    parser = ArgumentParser(description='Simple script to check existence of given branch for given set of components')
    parser.add_argument('--branch', action='append', required=True, help='branch to check')
    parser.add_argument('--comps-file', required=True, help='Path to file with list of components to check')
    parser.add_argument('--cleanup', action='store_true', default=False, help='If to cleanup the working dir on exit')

    conf = parser.parse_args(sys.argv[1:])
    main(conf)
