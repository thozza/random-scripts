#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, Tomas Hozza
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the project nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import logging

from argparse import ArgumentParser

import bugzilla


LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.StreamHandler())
LOGGER.setLevel(logging.INFO)


def main():
    bz = bugzilla.Bugzilla('bugzilla.redhat.com')

    # get all old FasTrack bugs
    query = bz.url_to_query('')
    bugs = bz.query(query)
    LOGGER.info("Bugs total: %d", len(bugs))

    new_flags = {
        'fast': 'X',
    }

    update = bz.build_update(keywords_add=['FastFix'])

    # update all old bugs
    for bug in bugs:
        LOGGER.info("Processing bug #%d", bug.id)
        bug.updateflags(new_flags)
        bz.update_bugs([bug.id], update)


if __name__ == '__main__':
    parser = ArgumentParser(description='Simple script to move bugs proposed for old releases to the given one.')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        default=False,
                        help='More verbose output')

    options = parser.parse_args()
    if options.verbose:
        LOGGER.setLevel(logging.DEBUG)

    LOGGER.debug('Arguments: %s', str(options))

    try:
        main()
    except RuntimeError as e:
        LOGGER.critical(str(e))
        sys.exit(1)
